<?php

// Vagrant local development vm.
$aliases['mccollective.mcdev'] = [
  'uri' => 'mccollective.mcdev',
  'root' => '/home/vagrant/docroot/web',
  'path-aliases' => [
    '%drush-script' => '/home/vagrant/docroot/bin/drush',
    '%dump-dir' => '/tmp',
  ],
];

if ('vagrant' !== getenv('USER')) {
  $aliases['mccollective.mcdev']['remote-host'] = 'mccollective.mcdev';
  $aliases['mccollective.mcdev']['remote-user'] = 'vagrant';
  $aliases['mccollective.mcdev']['ssh-options'] = '-o PasswordAuthentication=no -i ${HOME}/.vagrant.d/insecure_private_key';
}
