<?php

namespace Drush\Commands;

use Consolidation\AnnotatedCommand\CommandData;

/**
 * Project policy commands.
 *
 * Place this file in drush/Commands and add the following to drush/drush.yml:
 *
 * @code
 * drush:
 *  include:
 *    - './Commands'
 * @endcode
 */
class PolicyCommands extends DrushCommands {

  /**
   * Prevent remote databases from being dropped using drush sql:drop.
   *
   * Acquia and Pantheon standard prod alias names covered.
   *
   * @hook validate sql:drop
   *
   * @throws \Exception
   */
  public function sqlDropValidate(CommandData $commandData) {
    $target = $commandData->input()->getOption('target');
    if (strpos($target, 'prod') !== FALSE || strpos($target, 'live') !== FALSE || strpos($target, 'develop') !== FALSE) {
      throw new \Exception(dt('You may never drop a production database.'));
    }
  }

  /**
   * Prevent production databases from being overwritten using drush sql:sync.
   *
   * Acquia and Pantheon standard prod alias names covered.
   *
   * @hook validate sql:sync
   *
   * @throws \Exception
   */
  public function sqlSyncValidate(CommandData $commandData) {
    $target = $commandData->input()->getArgument('target');
    if (strpos($target, 'prod') !== FALSE || strpos($target, 'live') !== FALSE) {
      throw new \Exception(dt('You may never overwrite a production database.'));
    }
  }

  /**
   * Prevent production files from being overwritten using drush core:rsync.
   *
   * Acquia and Pantheon standard prod alias names covered.
   *
   * @hook validate core:rsync
   *
   * @throws \Exception
   */
  public function rsyncValidate(CommandData $commandData) {
    $target = $commandData->input()->getArgument('target');
    if (strpos($target, 'prod') !== FALSE || strpos($target, 'live') !== FALSE) {
      throw new \Exception(dt('You may never rsync to a production site.'));
    }
  }

}
